package com.example.joachim.robotcontroller;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SaveActionHTTP extends AsyncTask <String, Void, Void>{

    @Override
    protected Void doInBackground(String... args) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String webcontent = null;

        try {
            url = new URL("http://cabani.free.fr/ise/adddata.php?idproject=" + args[0]
                    + "&lux=" + args[1]
                    + "&timestamp=" + args[2]
                    + "&action=" + args[3]);
            System.out.println("URL : " + url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            webcontent = generateString(in);
            System.out.println("RESULT : " + webcontent);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    private String generateString(InputStream stream) {
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader buffer = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();
        try {
            String c;
            while((c = buffer.readLine()) != null) {
                sb.append(c + System.getProperty("line.separator"));
            }
            stream.close();
        } catch(IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

}
