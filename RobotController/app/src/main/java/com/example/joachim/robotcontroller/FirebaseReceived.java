package com.example.joachim.robotcontroller;

import android.app.Notification;
import android.app.NotificationManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseReceived extends FirebaseMessagingService {
    NotificationManager notificationManager;
    static int notificationID = 20;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("MSG", "From : " + remoteMessage.getFrom());
        Log.d("MSG", "Title : " + remoteMessage.getNotification().getTitle());
        Log.d("MSG", "Body : " + remoteMessage.getNotification().getBody());

        createNotification(remoteMessage.getNotification().getTitle(),
                            remoteMessage.getNotification().getBody());
    }

    @Override
    public void onMessageSent(String s) {
        Log.d("MSG", s);
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        Log.d("MSG", "ERREUR");
        super.onSendError(s, e);
    }

    private void createNotification(String title, String body) {
        Notification n  = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.warning)
                .setAutoCancel(true).build();

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, n);
        notificationID += 1;
        Log.d("NOTIF", "n: " + notificationID);
    }
}
