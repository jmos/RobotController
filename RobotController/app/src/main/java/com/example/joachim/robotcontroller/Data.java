package com.example.joachim.robotcontroller;

class Data {
    private static final Data ourInstance = new Data();

    private float lux;
    private boolean bluetoothConnected = false;
    private boolean screenBrightnessAuto = false;

    private Data() { }

    static Data getInstance() {
        return ourInstance;
    }

    public void setLux(final float lux) {
        this.lux = lux;
    }
    public float getLux() {
        return this.lux;
    }

    public boolean isBluetoothConnected() {
        return this.bluetoothConnected;
    }
    public void setBluetoothConnected(boolean bluetoothConnected) {
        this.bluetoothConnected = bluetoothConnected;
    }

    public int getAutoScreenBrightness() {
        int auto = (int) (lux / 1.1);
        return (auto <= 255) ? auto : 255;
    }

    public boolean isScreenBrightnessAuto() {
        return this.screenBrightnessAuto;
    }

    public void setScreenBrightnessAuto(boolean screenBrightnessAuto) {
        this.screenBrightnessAuto = screenBrightnessAuto;
    }

}
