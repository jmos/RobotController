package com.example.joachim.robotcontroller;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SettingsActivity extends AppCompatActivity implements BluetoothCallback {

    Context context;

    SensorManager sensorManager;
    Sensor lightSensor;

    Switch switchBluetooth, switchAutoLight;
    ToggleButton toggleButtonBluetoothConnection;
    SeekBar seekBarLight;
    ProgressBar spinner;
    TextView textViewLightLevel;

    private boolean settingsCanWrite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        switchBluetooth = findViewById(R.id.switchBluetooth);
        //TODO disconnect when toggle is off
        toggleButtonBluetoothConnection = findViewById(R.id.toggleButtonBluetoothConnection);
        switchAutoLight = findViewById(R.id.switchAutoLight);
        seekBarLight = findViewById(R.id.seekBarLight);
        spinner = findViewById(R.id.progressBar);
        textViewLightLevel = findViewById(R.id.textViewLightLevel);

        context = getApplicationContext();


        boolean b = BluetoothManager.getInstance().initializeBluetooth(this,
                "00001101-0000-1000-8000-00805F9B34FB", "RNBT-B236");

        switchBluetooth.setChecked(BluetoothManager.getInstance().isBluetoothOn());
        toggleButtonBluetoothConnection.setChecked(Data.getInstance().isBluetoothConnected());

        switchAutoLight.setChecked(Data.getInstance().isScreenBrightnessAuto());

        int visible = Data.getInstance().isScreenBrightnessAuto() ? View.INVISIBLE : View.VISIBLE;
        seekBarLight.setVisibility(visible);
        textViewLightLevel.setVisibility(visible);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        }

        switchBluetooth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    BluetoothManager.getInstance().turnOnBluetooth(SettingsActivity.this);
                }else{
                    BluetoothManager.getInstance().turnOffBluetooth();
                }
            }
        });


        toggleButtonBluetoothConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!BluetoothManager.getInstance().isBluetoothOn()) {
                    toggleButtonBluetoothConnection.setChecked(false);
                    Toast.makeText(SettingsActivity.this, "The BT device is OFF!",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if (toggleButtonBluetoothConnection.isChecked()) {
                    spinner.setVisibility(View.VISIBLE);
                    BluetoothManager.getInstance().startDiscover(SettingsActivity.this);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "CONNECTING...", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    BluetoothManager.getInstance().closeBluetooth(SettingsActivity.this);
                    spinner.setVisibility(View.INVISIBLE);
                    Data.getInstance().setBluetoothConnected(false);
                }
            }
        });



        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if(lightSensor == null) {
            switchAutoLight.setClickable(false);
            switchAutoLight.setText("Luminosité auto désactivée (pas de capteur)");
        } else {
            switchAutoLight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(switchAutoLight.isChecked()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            settingsCanWrite = Settings.System.canWrite(context);
                        }
                        if(!settingsCanWrite) {
                            Log.d("WRITE", "app doesn't have write permissions");
                            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                            startActivity(intent);
                        } else {
                            Data.getInstance().setScreenBrightnessAuto(true);
                            seekBarLight.setVisibility(View.INVISIBLE);
                            textViewLightLevel.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        Data.getInstance().setScreenBrightnessAuto(false);
                        seekBarLight.setVisibility(View.VISIBLE);
                        textViewLightLevel.setVisibility(View.VISIBLE);
                    }
                }
            });
        }


        seekBarLight.setProgress(Settings.System.getInt(context.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS,
                0));

        seekBarLight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress >= 0 && progress <= 255) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        settingsCanWrite = Settings.System.canWrite(context);
                    }

                    if(!settingsCanWrite) {
                        Log.d("WRITE", "app doesn't have write permissions");
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        startActivity(intent);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            settingsCanWrite = Settings.System.canWrite(context);
                    } else {
                        Log.d("WRITE", "app has write permissions");
                        Settings.System.putInt(context.getContentResolver(),
                                                Settings.System.SCREEN_BRIGHTNESS,
                                                progress);
                    }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    protected void onDestroy() {
        //when going back to main screen, we want to keep Connection
        //BluetoothManager.getInstance().closeBluetooth(this);
        super.onDestroy();
    }

    //on onActivityResult we must implement the CheckActivityResult of the BluetoothManager that
    // will check results about some request that were made about discovering and on/off
    // relations to bluetooth.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int resultBluetooth = BluetoothManager.getInstance().CheckActivityResult(requestCode,
                resultCode);

        if(resultBluetooth == BluetoothManager.BLUETOOTH_ON){
            switchBluetooth.setChecked(true);
        }else if(resultBluetooth == BluetoothManager.BLUETOOTH_OFF){
            switchBluetooth.setChecked(false);
        }else if(resultBluetooth == BluetoothManager.BLUETOOTH_DISCOVERY_LISTEN){
            spinner.setVisibility(View.VISIBLE);
            toggleButtonBluetoothConnection.setVisibility(View.INVISIBLE);
        }else if(resultBluetooth == BluetoothManager.BLUETOOTH_DISCOVERY_CANCELED){
            spinner.setVisibility(View.INVISIBLE);
            toggleButtonBluetoothConnection.setVisibility(View.VISIBLE);
        }

        super.onActivityResult(requestCode,resultCode,data);
    }

    //callback for when the device is connected or an error occurred. When the connection os ok,
    // it starts a new Activity for the message exchange
    @Override
    public void onBluetoothConnection(int returnCode) {
        if(returnCode == BluetoothManager.BLUETOOTH_CONNECTED){
            Toast.makeText(SettingsActivity.this, "CONNECTED",
                    Toast.LENGTH_SHORT).show();
            Data.getInstance().setBluetoothConnected(true);
        }else if(returnCode == BluetoothManager.BLUETOOTH_CONNECTED_ERROR){
            Toast.makeText(SettingsActivity.this, "ConnectionError",
                    Toast.LENGTH_SHORT).show();
            Data.getInstance().setBluetoothConnected(false);
        }
        spinner.setVisibility(View.INVISIBLE);
    }

    //callback for when the device is discoverable or if it ended the discoverable mode.
    @Override
    public void onBluetoothDiscovery(int returnCode) {
        if(returnCode == BluetoothManager.BLUETOOTH_DISCOVERABLE){
            spinner.setVisibility(View.VISIBLE);
            toggleButtonBluetoothConnection.setVisibility(View.INVISIBLE);

        }else if(returnCode == BluetoothManager.BLUETOOTH_CONNECTABLE ||
                returnCode == BluetoothManager.BLUETOOTH_NOT_CONNECTABLE){
            spinner.setVisibility(View.INVISIBLE);
            toggleButtonBluetoothConnection.setVisibility(View.VISIBLE);
        }
    }

    //callback for when the bluetooth receive some that, that we are going to deal in a separated
    // activity
    @Override
    public void onReceiveData(String data) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1001:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // --->
                    Log.d("BT", "permisionGranted");
                } else {
                    //TODO re-request
                    Log.d("BT", "permisionNOTGranted");
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
                }
                break;
        }
    }

}
