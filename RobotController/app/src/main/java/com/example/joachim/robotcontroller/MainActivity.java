package com.example.joachim.robotcontroller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements SensorEventListener, BluetoothCallback, View.OnClickListener {
    private static final String idProject = "3";

    ImageButton imageButtonRight, imageButtonLeft, imageButtonAccelerator,
            imageButtonBreak, imageButtonGearShift, imageButtonSettings, imageButtonInfo;
    ProgressBar progressBar;
    TextView textViewConnected;

    SensorManager sensorManager;
    Sensor lightSensor;

    JSONArray jsonArrayRecipient;

    int speed = 0;
    boolean isForward = true;

    private long last_timestamp = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //new FirebaseID();
        new FirebaseReceived();

        // the JSON array contains the token of the smartphone we want to send the notification to (the smartphone itself)
        jsonArrayRecipient = new JSONArray();
        jsonArrayRecipient.put(FirebaseInstanceId.getInstance().getToken());

        imageButtonAccelerator = findViewById(R.id.imageButtonAccelerator);
        imageButtonBreak = findViewById(R.id.imageButtonBreak);
        imageButtonRight = findViewById(R.id.imageButtonRight);
        imageButtonLeft = findViewById(R.id.imageButtonLeft);
        imageButtonGearShift = findViewById(R.id.imageButtonGearShift);
        imageButtonSettings = findViewById(R.id.imageButtonSettings);
        imageButtonInfo = findViewById(R.id.imageButtonInfo);
        progressBar = findViewById(R.id.progressBar);
        textViewConnected = findViewById(R.id.textViewConnected);

        progressBar.setProgress(speed);

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        imageButtonAccelerator.setOnClickListener(this);
        imageButtonAccelerator.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                speed = 100;
                progressBar.setProgress(speed);
                BluetoothManager.getInstance().sendData(MainActivity.this, "i\r");
                saveAction("accelerer_max");

                return true;    //prevent the OnClickListener to be called
            }
        });

        imageButtonBreak.setOnClickListener(this);
        imageButtonBreak.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                speed = 0;
                progressBar.setProgress(speed);
                BluetoothManager.getInstance().sendData(MainActivity.this, "s\r");
                saveAction("arrêt");

                return true;
            }
        });

        imageButtonGearShift.setOnClickListener(this);
        imageButtonRight.setOnClickListener(this);
        imageButtonLeft.setOnClickListener(this);
        imageButtonSettings.setOnClickListener(this);
        imageButtonInfo.setOnClickListener(this);
    }

    private void saveAction(String action) {
        if(!Data.getInstance().isBluetoothConnected())
            return;

        String lux = Float.toString(Data.getInstance().getLux());

        Long timestampLong = System.currentTimeMillis()/1000;
        String timestamp = timestampLong.toString();

        new SaveActionHTTP().execute(idProject, lux, timestamp, action);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //en commentaire pour que le capteur reste actif et que les autres activitées aient acces aux données de luminosité
        //sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_UI);

        if(Data.getInstance().isBluetoothConnected()) {
            textViewConnected.setVisibility(View.INVISIBLE);
            BluetoothManager.getInstance().startReadingData(this);
        }
        else {
            textViewConnected.setVisibility(View.VISIBLE);
            BluetoothManager.getInstance().stopReadingData();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_LIGHT) {

            if(event.values[0] - Data.getInstance().getLux() < 15
                    && event.values[0] - Data.getInstance().getLux() > -15)
                return;     //not enough change in luminosity

            Data.getInstance().setLux(event.values[0]);

            if(Data.getInstance().isScreenBrightnessAuto()) {
                Settings.System.putInt(getApplicationContext().getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS,
                        Data.getInstance().getAutoScreenBrightness());
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void onBluetoothConnection(int returnCode) { }

    @Override
    public void onBluetoothDiscovery(int returnCode) { }

    @Override
    public void onReceiveData(String data) {
        final String finalData = data;

        if(finalData.contains("obstacle")) {
            long current_timestamp = System.currentTimeMillis()/1000;

            if((current_timestamp - last_timestamp) > 3) {      //prevent from having 100 notifications / second
                last_timestamp = current_timestamp;
                sendMessageFirebase(jsonArrayRecipient, "OBSTACLE", "an obstacle has been detected");
            }

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageButtonAccelerator:
                if((speed += 5) > 100) {
                    speed = 100;
                }
                progressBar.setProgress(speed);
                BluetoothManager.getInstance().sendData(MainActivity.this, "o\r");
                saveAction("accelerer");
                break;

            case R.id.imageButtonBreak:
                if((speed -= 5) < 0) {
                    speed = 0;
                }
                progressBar.setProgress(speed);
                BluetoothManager.getInstance().sendData(MainActivity.this, "l\r");
                saveAction("freiner");
                break;

            case R.id.imageButtonGearShift:
                if(speed > 60) {
                    Toast.makeText(MainActivity.this, "Speed too high", Toast.LENGTH_SHORT).show();
                    return;
                }
                isForward = !isForward;
                BluetoothManager.getInstance().sendData(MainActivity.this, "a\r");

                if(isForward) {
                    imageButtonGearShift.setImageResource(R.drawable.gear_shift);
                    saveAction("marche avant");
                } else {
                    imageButtonGearShift.setImageResource(R.drawable.gear_shift2);
                    saveAction("marche arriere");
                }
                break;
            case R.id.imageButtonRight:
                BluetoothManager.getInstance().sendData(MainActivity.this, "m\r");
                saveAction("tourner droite");
                break;

            case R.id.imageButtonLeft:
                BluetoothManager.getInstance().sendData(MainActivity.this, "k\r");
                saveAction("tourner gauche");
                break;

            case R.id.imageButtonSettings:
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;

            case R.id.imageButtonInfo:
                Intent intent2 = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent2);
                break;

            default:
                break;
        }
    }

    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final String SERVER_KEY = "AAAAAGEAMS8:APA91bHp8xRJNN4XcAKKLtwTFXto08_nRauUjMm42xn1_8GnFrPVa5FbPf0rGG0O62XypzFBuGxmViM4szK8cM44nyhp0xi7a33T82o59h3IGzJVbn5KRxe9a4jtnsUNPIwZjFrskAoz";
    OkHttpClient mClient = new OkHttpClient();

    @SuppressLint("StaticFieldLeak")
    public void sendMessageFirebase(final JSONArray recipients, final String title, final String body) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", body);
                    notification.put("title", title);

                    JSONObject data = new JSONObject();
                    root.put("notification", notification);
                    root.put("data", data);
                    root.put("registration_ids", recipients);

                    String result = postToFCM(root.toString());
                    Log.d("FCM", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    //Toast.makeText(MainActivity.this, "Message Success: " + success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(MainActivity.this, "Message Failed, Unknown error occurred.", Toast.LENGTH_LONG).show();
                    Log.d("JSON", result);
                }
            }
        }.execute();
    }

    String postToFCM(String bodyString) throws IOException {
        RequestBody body = RequestBody.create(JSON, bodyString);

        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + SERVER_KEY)
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }
}
